Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: no-info-found
License: GPL-3

Files: Makefile.in
 ansi_stdlib.h
 bind.c
 callback.c
 chardefs.h
 colors.c
 colors.h
 compat.c
 complete.c
 configure.ac
 display.c
 emacs_keymap.c
 funmap.c
 histexpand.c
 histfile.c
 histlib.h
 history.c
 history.h
 histsearch.c
 input.c
 isearch.c
 keymaps.c
 keymaps.h
 kill.c
 macro.c
 mbutil.c
 misc.c
 nls.c
 parens.c
 parse-colors.c
 posixdir.h
 posixjmp.h
 posixselect.h
 posixstat.h
 posixtime.h
 readline.c
 readline.h
 rlconf.h
 rldefs.h
 rlmbutil.h
 rlprivate.h
 rlshell.h
 rlstdc.h
 rltty.c
 rltty.h
 rltypedefs.h
 rlwinsize.h
 savestring.c
 search.c
 shell.c
 signals.c
 tcap.h
 terminal.c
 text.c
 tilde.c
 tilde.h
 undo.c
 util.c
 vi_keymap.c
 vi_mode.c
 xfree.c
 xmalloc.c
 xmalloc.h
Copyright: 1985-2023, Free Software Foundation, Inc.
License: GPL-3+

Files: configure
Copyright: 1992-1996, 1998-2017, 2020, 2021, Free Software Foundation
License: FSFUL

Files: debian/*
Copyright: 1999-2023, Matthias Klose <doko@debian.org>
License: GPL-3

Files: debian/patches/fix-rl_do_undo-crash.diff
Copyright: 1989-2009, 2021-2023, Free Software Foundation, Inc.
License: GPL-3

Files: debian/patches/rlfe-tinfo.diff
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL

Files: doc/*
Copyright: 1988-2022, Free Software Foundation, Inc.
License: GFDL-1.3+

Files: doc/Makefile.in
 doc/texi2dvi
 doc/texi2html
 doc/texinfo.tex
Copyright: 1985-2023, Free Software Foundation, Inc.
License: GPL-3+

Files: examples/Inputrc
 examples/Makefile.in
 examples/fileman.c
 examples/hist_erasedups.c
 examples/hist_purgecmd.c
 examples/histexamp.c
 examples/manexamp.c
 examples/rl-timeout.c
 examples/rl.c
 examples/rlcat.c
 examples/rlevent.c
 examples/rltest.c
 examples/rlversion.c
Copyright: 1985-2023, Free Software Foundation, Inc.
License: GPL-3+

Files: examples/readlinebuf.h
Copyright: 2001, Dimitris Vyzovitis [vyzo@media.mit.edu]
License: GPL-2+

Files: examples/rlfe/config.h.in
Copyright: 2004, Per Bothner <per@bothner.com>
 1993-2000, Juergen Weigert (jnweiger@immd4.informatik.uni-erlangen.de)
 1987, Oliver Laumann
License: GPL-2+

Files: examples/rlfe/configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL

Files: examples/rlfe/extern.h
 examples/rlfe/os.h
 examples/rlfe/pty.c
Copyright: 1993-2002, Juergen Weigert (jnweiger@immd4.informatik.uni-erlangen.de)
 1987, Oliver Laumann
License: GPL-2+

Files: examples/rlfe/rlfe.c
Copyright: 1999, 2004, Per Bothner <per@bothner.com>
License: GPL-2+

Files: m4/*
Copyright: 2000-2002, 2006, 2008-2014, 2016, 2019, Free Software
License: FSFULLR

Files: parse-colors.h
Copyright: 1985, 1988, 1990, 1991, 1995-2010, 2012, Free Software Foundation
License: GPL-3+

Files: shlib/*
Copyright: 1985-2023, Free Software Foundation, Inc.
License: GPL-3+

Files: support/config.rpath
Copyright: 1996-2018, Free Software Foundation, Inc.
License: FSFULLR

Files: support/install.sh
Copyright: 1991, the Massachusetts Institute of Technology
License: HPND-sell-variant

Files: support/mkdirs
 support/mkdist
 support/shobj-conf
Copyright: 1985-2023, Free Software Foundation, Inc.
License: GPL-3+

Files: aclocal.m4 doc/history.0 doc/history.3 doc/readline.0 doc/readline.3 examples/excallback.c
Copyright:
 Copyright (C) 1987-2022 Free Software Foundation, Inc.
License: GPL-3+
    Readline is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
 .
    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 .
    You should have received a copy of the GNU General Public License
    along with Readline.  If not, see <http://www.gnu.org/licenses/>.

Files: doc/fdl.texi doc/hstech.texi doc/hsuser.texi doc/rltech.texi doc/rluser.texi doc/version.texi
Copyright:
 Copyright (C) 1988-2015 Free Software Foundation, Inc.
License: GFDL-NIV-1.3+
    Permission is granted to copy, distribute and/or modify this
    document under the terms of the GNU Free Documentation License,
    Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and
    no Back-Cover Texts.  A copy of the license is included in the
    section entitled "GNU Free Documentation License".

Files: examples/rl-fgets.c
Copyright:
 Copyright (C) 1987-2022 Free Software Foundation, Inc.
 Copyright (c) 2001 by Dimitris Vyzovitis [vyzo@media.mit.edu]
 Copyright (C) 1999 Jeff Solomon (examples/excallback.c)
 Copyright (C) 2003-2004 Harold Levy (examples/rl-fgets.c)
 Copyright (c) 1993-2002 Juergen Weigert (jnweiger@immd4.informatik.uni-erlangen.de)
 Copyright (c) 1993-2002 Michael Schroeder (mlschroe@immd4.informatik.uni-erlangen.de)
 Copyright (C) 1987 Oliver Laumann (examples/rlfe)
 Copyright (C) 2004, 1999 Per Bothner
 Copyright (C) 2000-2007 Hans Lub
 Copyright (C) 1999-2001 Geoff Wing
 Copyright (C) Damian Ivereigh 2000
License: GPL-2+
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2, or (at your option)
    any later version.
 .
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 .
    You should have received a copy of the GNU General Public License
    along with Readline.  If not, see <http://www.gnu.org/licenses/>.
